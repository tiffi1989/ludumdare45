﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawn : MonoBehaviour {

  public GameObject prefab;
  public float time;

	// Use this for initialization
	void Start () {
    Invoke("Spawn", time);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  private void Spawn()
  {
    Instantiate(prefab, transform.position, transform.rotation);
  }
}
