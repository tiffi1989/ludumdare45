﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Start : MonoBehaviour
{

  public float speed;
  private bool startGame;
  // Update is called once per frame
  void FixedUpdate()
  {
    if (transform.position.x > -4f)
      transform.position = new Vector3(transform.position.x - speed, transform.position.y, transform.position.z);
    else
    {
      if(!startGame)
      {
        startGame = true;
        Invoke("Begin", 2f);
      }
    }
    
  }

  private void Begin()
  {
    SceneManager.LoadScene("Map1");
  }
}
