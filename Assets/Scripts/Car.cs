﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car
{
  public float maxSpeed = 0;
  public float acceleration = 0;
  public float steering = 0;

  public Car(float maxSpeed, float acceleration, float steering)
  {
    this.maxSpeed = maxSpeed;
    this.acceleration = acceleration;
    this.steering = steering;
  }

  public float GetMaxSpeed()
  {
    return maxSpeed;
  }

  public float GetAcceleration()
  {
    return acceleration;
  }

  public float getSteering()
  {
    return steering;
  }

}
