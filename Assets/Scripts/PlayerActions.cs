﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{

  public float carEnterDistance;
  private PlayerState playerState;
  private Rigidbody2D rig;
  private Vector2 lastVelocity;
  private float timer;
  private bool disableWayPoints;
  private float invinceTimer;
  public AudioClip crash;
  public AudioClip door;
  public AudioClip screem;
  private AudioSource[] audioSources;

  // Use this for initialization
  void Start()
  {
    playerState = GetComponent<PlayerState>();
    rig = GetComponent<Rigidbody2D>();
    audioSources = GetComponents<AudioSource>();
  }

  // Update is called once per frame
  void FixedUpdate()
  {
    timer += Time.deltaTime;
    if (playerState.isDriving)
      invinceTimer += Time.deltaTime;
    else invinceTimer = 0;
    lastVelocity = rig.velocity;
    if (Input.GetKeyDown(KeyCode.Return) && !playerState.isDriving)
    {
      EnterCar();
    }
    else if (Input.GetKeyDown(KeyCode.Escape) && playerState.isDriving)
    {
      LeaveCar();
    }

    if (playerState.isFlying && rig.velocity.magnitude < 1 && timer > 2 && playerState.lives > 0)
    {
      playerState.isFlying = false;
      timer = 0;
      GetComponent<Animator>().SetBool("Flying", false);
      gameObject.layer = 10;
      GetComponent<CircleCollider2D>().isTrigger = false;


    }

  }


  private void EnterCar()
  {
    GameObject closestCar = FindClosestEmptyCar();
    if (closestCar)
    {
      audioSources[0].clip = door;
      audioSources[0].Play();
      transform.rotation = closestCar.transform.rotation;
      closestCar.transform.parent = transform;
      closestCar.GetComponent<CarBehaviour>().empty = false;
      playerState.SetNewCar(closestCar.GetComponent<CarBehaviour>().GetCar());
      playerState.currentCarTransform = closestCar.transform;
      playerState.isDriving = true;

      transform.position = closestCar.transform.position;
      transform.rotation = closestCar.transform.rotation;
      rig.velocity = new Vector2();

    }
  }

  private void LeaveCar()
  {
    LeaveCar(false);

  }
  private void LeaveCar(bool flying)
  {
    audioSources[0].clip = door;
    audioSources[0].Play();
    playerState.isDriving = false;
    playerState.RemoveCar();

    playerState.currentCarTransform.GetComponent<CarBehaviour>().empty = true;
    playerState.currentCarTransform.parent = null;
    if (!flying)
      transform.position = new Vector3(transform.position.x - 2f, transform.position.y, transform.position.z);
  }

  public GameObject FindClosestEmptyCar()
  {
    GameObject[] gos;
    gos = GameObject.FindGameObjectsWithTag("Car");
    GameObject closest = null;
    float distance = Mathf.Infinity;
    Vector3 position = transform.position;
    foreach (GameObject go in gos)
    {
      CarBehaviour cb = go.GetComponent<CarBehaviour>();

      Vector3 diff = go.transform.position - position;
      float curDistance = diff.sqrMagnitude;
      if (curDistance < distance && cb && cb.empty)
      {
        closest = go;
        distance = curDistance;
      }
    }

    if (carEnterDistance > distance)
      return closest;
    else return null;
  }
  private void EnableWaypoints()
  {
    disableWayPoints = false;
  }

  private void OnTriggerEnter2D(Collider2D collision)
  {
    if ((collision.gameObject.CompareTag("RoundPoint") || collision.gameObject.CompareTag("End")) && !disableWayPoints)
    {
      playerState.amountOfWaypointsThisLap++;
      disableWayPoints = true;
      Invoke("EnableWaypoints", 1f);
    }
    if (collision.gameObject.CompareTag("End") && playerState.amountOfWaypointsThisLap > 8)
    {
      playerState.lap++;
      playerState.lives = 3;
      playerState.amountOfWaypointsThisLap = 0;
    }
  }

  private void OnCollisionEnter2D(Collision2D collision)
  {
    Debug.Log(collision.gameObject.tag);
    if (invinceTimer > 2f && ((collision.gameObject.CompareTag("Enemy") && collision.gameObject.GetComponent<EnemyController>().isDriving && !playerState.isDriving && !playerState.isFlying) ||
      (((collision.gameObject.CompareTag("Obstacle") || collision.gameObject.CompareTag("Destroyable") || collision.gameObject.CompareTag("Car")) && lastVelocity.magnitude > 6 && playerState.isDriving) ||
      (collision.gameObject.CompareTag("Enemy") && collision.gameObject.GetComponent<EnemyController>().isDriving &&
      lastVelocity.magnitude - collision.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude > 6))))
    {
      audioSources[0].clip = crash;
      audioSources[0].Play();
      audioSources[1].clip = screem;
      audioSources[1].Play();
      if (playerState.isDriving)
        LeaveCar(true);
      playerState.isFlying = true;
      gameObject.layer = 9;
      rig.AddForce(lastVelocity.normalized * 700);
      GetComponent<Animator>().SetBool("Flying", true);
      float angle = Mathf.Atan2(lastVelocity.y, lastVelocity.x) * Mathf.Rad2Deg;
      transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
      timer = 0;
      playerState.lives--;
    }
    if (collision.gameObject.CompareTag("Destroyable"))
      GameObject.Destroy(collision.gameObject);
  }
}
