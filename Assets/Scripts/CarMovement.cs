﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{


  private Rigidbody2D rb;
  private float currentSpeed;

  private AudioSource audioSource;


  private PlayerState playerState;
  public AudioClip tires;

  // Use this for initialization
  void Start()
  {
    rb = GetComponent<Rigidbody2D>();
    playerState = GetComponent<PlayerState>();
    audioSource = GetComponent<AudioSource>();
  }

  // Update is called once per frame
  void FixedUpdate()
  {
    if (playerState.currentCar != null && playerState.isDriving)
      MovePlayer();
  }

  void MovePlayer()
  {
    // Get input
    float h = -Input.GetAxis("Horizontal");
    float v = Input.GetAxis("Vertical");

    if(v < 0 && !audioSource.isPlaying)
    {
      audioSource.clip = tires;
      audioSource.Play();
    }

    // Calculate speed from input and acceleration (transform.up is forward)
    Vector2 speed = transform.up * (v * playerState.acceleration);
    rb.AddForce(speed);

    // Create car rotation
    float direction = Vector2.Dot(rb.velocity, rb.GetRelativeVector(Vector2.up));
    if (direction >= 0.0f)
    {
      rb.rotation += h * playerState.steering * (rb.velocity.magnitude / playerState.maxSpeed);
    }
    else
    {
      rb.rotation -= h * playerState.steering * (rb.velocity.magnitude / playerState.maxSpeed);
    }

    // Change velocity based on rotation
    float driftForce = Vector2.Dot(rb.velocity, rb.GetRelativeVector(Vector2.left)) * 2.0f;
    Vector2 relativeForce = Vector2.right * driftForce;
    rb.AddForce(rb.GetRelativeVector(relativeForce));

    // Force max speed limit
    if (rb.velocity.magnitude > playerState.maxSpeed)
    {
      rb.velocity = rb.velocity.normalized * playerState.maxSpeed;
    }

    currentSpeed = rb.velocity.magnitude;
  }

 
}
