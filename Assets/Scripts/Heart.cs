﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Heart : MonoBehaviour {

  PlayerState ps;
  public int heartNumber;
  private Image img;
	// Use this for initialization
	void Start () {
    ps = GameObject.Find("Player").GetComponent<PlayerState>();
    img = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
    if (ps.lives < heartNumber)
    {
      img.enabled = false;
    }
    else img.enabled = true;
	}
}
