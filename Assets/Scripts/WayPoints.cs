﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoints : MonoBehaviour
{

  private WayPoint[] allWaypoints;
  public WayPoint firstWayPoint;

  // Use this for initialization
  void Start()
  {
    allWaypoints = GetComponentsInChildren<WayPoint>();
    Array.Sort(allWaypoints, delegate (WayPoint w1, WayPoint w2)
    {
      return w1.orderNumber.CompareTo(w2.orderNumber);
    });
  }

  // Update is called once per frame
  void Update()
  {

  }

  public WayPoint GetNextWaypoint(int orderNumber)
  {
    if (allWaypoints != null)
    {
      foreach (WayPoint wayPoint in allWaypoints)
      {
        if (wayPoint.orderNumber > orderNumber)
          return wayPoint;
      }
      return allWaypoints[0];
    }
    else return firstWayPoint;

  }

}
