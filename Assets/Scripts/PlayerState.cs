﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerState : MonoBehaviour {
  public Car currentCar;
  public Transform currentCarTransform;

  public bool isDriving, isFlying;
  public float maxSpeed = 0;
  public float acceleration = 0;
  public float steering = 0;
  public int lives = 3;
  private bool dead;
  public int amountOfWaypointsThisLap;
  public int lap = 1;
  private CircleCollider2D circleCollider2D;
  // Use this for initialization
  void Start () {
    circleCollider2D = GetComponent<CircleCollider2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(isDriving)
    {
      circleCollider2D.enabled = false;
      currentCarTransform.position = transform.position;
    }
    else
    {
      circleCollider2D.enabled = true;
    }
    if(lives == 0 && !dead)
    {
      dead = true;
      Invoke("End", 3);
    }
	}

  private void End()
  {
    SceneManager.LoadScene("Dead");
  }


  public void SetNewCar(Car car)
  {
    currentCar = car;
    acceleration = car.GetAcceleration();
    maxSpeed = car.GetMaxSpeed();
    steering = car.getSteering();

  }

  public void RemoveCar()
  {
    currentCar = null;
  }
}
