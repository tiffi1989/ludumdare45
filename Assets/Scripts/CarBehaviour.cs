﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBehaviour : MonoBehaviour
{

  public float maxSpeed = 0;
  public float acceleration = 0;
  public float steering = 0;
  public bool empty = true;
  private Rigidbody2D rb;
  public Transform tireFL, tireFR;
  public Animator[] animators;
  private Vector2 lastVelocity;
  public ParticleSystem[] dirtParts;
  public bool drivenByEnemy;
  private AudioSource audioSource;
  // Use this for initialization
  void Start()
  {
    rb = GameObject.Find("Player").GetComponent<Rigidbody2D>();
    audioSource = GetComponent<AudioSource>();
  }

  // Update is called once per frame
  void FixedUpdate()
  {


    if (!empty && GetComponentInParent<Rigidbody2D>().velocity.magnitude < 0.3f)
      foreach (Animator ani in animators)
      {
        ani.SetBool("Driving", false);
      }
    if (!empty)
    {
      if (GetComponentInParent<Rigidbody2D>().velocity.magnitude > 0.3f)
      {
        foreach (Animator ani in animators)
        {
          ani.SetBool("Driving", true);
        }

      }
      Dirt();
      if (!drivenByEnemy)
        MoveForPlayer();
      else
        MoveForEnemy();
    }

  }

  private void MoveForEnemy()
  {

  }

  private void MoveForPlayer()
  {



    if (Input.GetAxis("Horizontal") < -0.3f || Input.GetAxis("Horizontal") > 0.3f)
    {
      if (Input.GetAxis("Horizontal") < 0)
      {
        tireFL.localRotation = Quaternion.AngleAxis(30f, Vector3.forward);
        tireFR.localRotation = Quaternion.AngleAxis(30f, Vector3.forward);
      }
      else
      {
        tireFL.localRotation = Quaternion.AngleAxis(-30f, Vector3.forward);
        tireFR.localRotation = Quaternion.AngleAxis(-30f, Vector3.forward);
      }

    }
    else
    {
      tireFL.localRotation = new Quaternion();
      tireFR.localRotation = new Quaternion();
    }
  }

  private void Dirt()
  {
    Vector2 currentAcc = (rb.velocity - lastVelocity) / Time.fixedDeltaTime;

    if (rb.velocity.magnitude > lastVelocity.magnitude && currentAcc.magnitude > 7 && Input.GetAxis("Vertical") > 0)
    {
      foreach (ParticleSystem ps in dirtParts)
      {
        ps.Play();
        if (false)
          audioSource.Play();
      }
    }
    else
    {
      foreach (ParticleSystem ps in dirtParts)
      {
        ps.Stop();


      }
    }
    lastVelocity = rb.velocity;

  }

  public Car GetCar()
  {
    return new Car(maxSpeed, acceleration, steering);
  }
}
