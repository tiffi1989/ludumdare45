﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour {

  private float timer;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    if(Input.GetKey(KeyCode.R))
    {
      timer += Time.deltaTime;
      if (timer > 1) SceneManager.LoadScene("Map1");
    }

  }
}
