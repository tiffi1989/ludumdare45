﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
  public Car currentCar;

  public bool isDriving, isFlying, moveToCar, disableWayPoints;
  public GameObject currentDestination;
  private float timer = 1.5f;
  public float maxSpeed = 0;
  public float acceleration = 0;
  public float steering = 0;
  private Vector2 lastVelocity;
  private Rigidbody2D rig;
  private Transform currentCarTransform;
  public int amountOfWaypointsThisLap;
  public int lap = 1;
  private float invinceTimer;
  public AudioClip crash;
  public AudioClip door;
  public AudioClip screem;
  private AudioSource[] audioSources;
  // Use this for initialization
  void Start()
  {
    rig = GetComponent<Rigidbody2D>();
    audioSources = GetComponents<AudioSource>();

  }

  // Update is called once per frame
  void FixedUpdate()
  {
    if (isDriving)
      invinceTimer++;
    else invinceTimer = 0;
    if (isDriving)
      lastVelocity = rig.velocity;
    else lastVelocity = new Vector2();
    timer += Time.deltaTime;
    if (!isDriving && !isFlying && (timer > 2 || (currentDestination != null && currentDestination.GetComponent<CarBehaviour>() != null && !currentDestination.GetComponent<CarBehaviour>().empty)))
    {
      timer = 0;
      currentDestination = FindClosestEmptyCar();
      moveToCar = true;
    }
    if (moveToCar && currentDestination != null && Vector3.Distance(transform.position, currentDestination.transform.position) < 3.5)
    {
      EnterCar();
    }

    if (isFlying && rig.velocity.magnitude < 1 && timer > 2)
    {
      isFlying = false;
      GetComponent<Animator>().SetBool("Flying", false);

    }
  }

  private void EnterCar()
  {
    if (!currentDestination.GetComponent<CarBehaviour>().empty)
      return;
    audioSources[0].clip = door;
    audioSources[0].Play();
    transform.rotation = currentDestination.transform.rotation;
    currentDestination.GetComponent<CarBehaviour>().empty = false;
    currentDestination.GetComponent<CarBehaviour>().drivenByEnemy = true;

    SetNewCar(currentDestination.GetComponent<CarBehaviour>().GetCar());
    isDriving = true;

    transform.position = currentDestination.transform.position;
    transform.rotation = currentDestination.transform.rotation;
    currentDestination.transform.parent = transform;
    currentCarTransform = currentDestination.transform;
    GetComponent<Rigidbody2D>().velocity = new Vector2();
    moveToCar = false;
  }

  public void SetNewCar(Car car)
  {
    currentCar = car;
    acceleration = car.GetAcceleration();
    maxSpeed = car.GetMaxSpeed();
    steering = car.getSteering();

  }

  private void EnableWaypoints()
  {
    disableWayPoints = false;
  }




  public GameObject FindClosestEmptyCar()
  {
    GameObject[] gos;
    gos = GameObject.FindGameObjectsWithTag("Car");
    GameObject closest = null;
    float distance = Mathf.Infinity;
    Vector3 position = transform.position;
    foreach (GameObject go in gos)
    {
      CarBehaviour cb = go.GetComponent<CarBehaviour>();

      Vector3 diff = go.transform.position - position;
      float curDistance = diff.sqrMagnitude;
      if (curDistance < distance && cb && cb.empty)
      {
        closest = go;
        distance = curDistance;
      }
    }

    return closest;
  }

  private void LeaveCar(bool flying)
  {
    isDriving = false;
    currentCar = null;
    currentCarTransform.GetComponent<CarBehaviour>().empty = true;
    currentCarTransform.parent = null;
    if (!flying)
      transform.position = new Vector3(transform.position.x - 2f, transform.position.y, transform.position.z);
  }
  private void OnTriggerEnter2D(Collider2D collision)
  {

    if ((collision.gameObject.CompareTag("RoundPoint") || collision.gameObject.CompareTag("End")) && !disableWayPoints)
    {
      amountOfWaypointsThisLap++;
      disableWayPoints = true;
      Invoke("EnableWaypoints", 1f);
    }
    if (collision.gameObject.CompareTag("End") && amountOfWaypointsThisLap > 8)
    {
      lap++;
      amountOfWaypointsThisLap = 0;
    }
  }
  private void OnCollisionEnter2D(Collision2D collision)
  {

    if (invinceTimer > 2f && (((collision.gameObject.CompareTag("Obstacle") || collision.gameObject.CompareTag("Destroyable") || collision.gameObject.CompareTag("Car") || collision.gameObject.CompareTag("Player")) && lastVelocity.magnitude > 6) ||
      (collision.gameObject.CompareTag("Enemy") && collision.gameObject.GetComponent<EnemyController>().isDriving &&
      lastVelocity.magnitude - collision.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude > 6)))
    {
      audioSources[0].clip = crash;
      audioSources[0].Play();
      audioSources[1].clip = screem;
      audioSources[1].Play();
      LeaveCar(true);
      isFlying = true;
      gameObject.layer = 9;
      rig.AddForce(lastVelocity.normalized * 700);
      GetComponent<Animator>().SetBool("Flying", true);
      float angle = Mathf.Atan2(lastVelocity.y, lastVelocity.x) * Mathf.Rad2Deg;
      transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
      timer = 0;
      lastVelocity = new Vector2();


    }
    if (collision.gameObject.CompareTag("Destroyable"))
      GameObject.Destroy(collision.gameObject);
  }
}
