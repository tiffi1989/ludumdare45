﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
  public float speed;
  private float curSpeed;
  private float maxSpeed;

  private Rigidbody2D rig;
  private Animator ani;

  private PlayerState playerState;


  void Start()
  {
    rig = GetComponent<Rigidbody2D>();
    playerState = GetComponent<PlayerState>();
    ani = GetComponent<Animator>();
  }

  void FixedUpdate()
  {
    if (!playerState.isDriving && !playerState.isFlying)
      MovePlayer();
  }

  void MovePlayer()
  {

    curSpeed = speed;
    maxSpeed = curSpeed;
    rig.velocity = new Vector2(Mathf.Lerp(0, Input.GetAxis("Horizontal") * curSpeed, 0.8f), Mathf.Lerp(0, Input.GetAxis("Vertical") * curSpeed, 0.8f));
    float angle = Mathf.Atan2(rig.velocity.y, rig.velocity.x) * Mathf.Rad2Deg;
    if (rig.velocity.magnitude > 0.1f)
    {
      transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
      ani.SetBool("Running", true);
      
    } else
    {
      ani.SetBool("Running", false);
    }

  }
}
