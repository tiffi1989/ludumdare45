﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDriving : MonoBehaviour
{



  private Rigidbody2D rb;
  private float currentSpeed;

  private WayPoints wayPoints;


  private EnemyController enemyController;
  private WayPoint nextWayPoint;

  // Use this for initialization
  void Start()
  {
    rb = GetComponent<Rigidbody2D>();
    enemyController = GetComponent<EnemyController>();
    wayPoints = GameObject.Find("WayPoints").GetComponent<WayPoints>();
    nextWayPoint = wayPoints.GetNextWaypoint(-1);
  }

  // Update is called once per frame
  void FixedUpdate()
  {
    if (enemyController.currentCar != null && enemyController.isDriving)
      MoveEnemy();

    float distanceToWaypoint = Vector3.Distance(transform.position, nextWayPoint.transform.position);
    if(distanceToWaypoint < 6)
    {
      nextWayPoint = wayPoints.GetNextWaypoint(nextWayPoint.orderNumber);
    }
  }

  void MoveEnemy()
  {
    // Get input

    // Calculate speed from input and acceleration (transform.up is forward)
    Vector2 speed = (nextWayPoint.transform.position - transform.position).normalized *  enemyController.acceleration;
    rb.AddForce(speed);

    // Create car rotation
    float direction = Vector2.Dot(rb.velocity, new Vector2(nextWayPoint.transform.position.x, nextWayPoint.transform.position.y));


    // Force max speed limit
    if (rb.velocity.magnitude > enemyController.maxSpeed)
    {
      rb.velocity = rb.velocity.normalized * enemyController.maxSpeed;
    }

    currentSpeed = rb.velocity.magnitude;
    float angle = Mathf.Atan2(rb.velocity.y, rb.velocity.x) * Mathf.Rad2Deg;
    transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

  }

}
