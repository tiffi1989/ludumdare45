﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoving : MonoBehaviour
{
  public float speed;
  private float curSpeed;
  private float maxSpeed;

  private Rigidbody2D rig;
  private Animator ani;

  private EnemyController enemyController;


  void Start()
  {
    rig = GetComponent<Rigidbody2D>();
    enemyController = GetComponent<EnemyController>();
    ani = GetComponent<Animator>();
  }

  void FixedUpdate()
  {
    if (!enemyController.isDriving && !enemyController.isFlying && enemyController.moveToCar && enemyController.currentDestination != null)
      MoveEnemy();
  }

  void MoveEnemy()
  {

    curSpeed = speed;
    maxSpeed = curSpeed;
    Vector3 targetVector = enemyController.currentDestination.transform.position - transform.position;
    rig.velocity = new Vector2(Mathf.Lerp(0, targetVector.normalized.x * curSpeed, 0.8f), Mathf.Lerp(0, targetVector.normalized.y * curSpeed, 0.8f));


    float angle = Mathf.Atan2(rig.velocity.y, rig.velocity.x) * Mathf.Rad2Deg;
    if (rig.velocity.magnitude > 0.1f)
    {
      transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }
  

  }
}
