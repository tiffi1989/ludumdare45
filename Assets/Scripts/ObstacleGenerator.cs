﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{

  public GameObject Obstacle;
  private float timer;
  public float spawnRate;
  public int maxObs;

  // Use this for initialization
  void Start()
  {
    if (spawnRate == 0)
      spawnRate = Random.Range(5f, 15f);
  }

  // Update is called once per frame
  void Update()
  {
    timer += Time.deltaTime;

    if (timer > spawnRate)
    {
      timer = 0;
      GameObject[] allObs = GameObject.FindGameObjectsWithTag("Destroyable");
      if (allObs.Length < maxObs)
        Instantiate(Obstacle, new Vector3(transform.position.x + Random.Range(-10f, 10f), transform.position.y + Random.Range(-10f, 10f), transform.position.z), Quaternion.identity);
    }
  }
}
