﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cow : MonoBehaviour {

  public Vector3 nextDestination;
  private Vector3 center = new Vector3(-55.59575f, -16.09772f, 0);
  private Vector3 size = new Vector3(45, 35, 0);
  private Rigidbody2D rig;
  public float maxSpeed, minSpeed;
  private float speed;

	// Use this for initialization
	void Start () {
    rig = GetComponent<Rigidbody2D>();
    speed = Random.Range(minSpeed, maxSpeed);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
    if (Vector3.Distance(nextDestination, transform.position) < 2)
      nextDestination = new Vector3(0, 0, 0);
    if (nextDestination.x == 0 && nextDestination.y == 0)
      GenerateNextDestination();

    float angle = Mathf.Atan2(rig.velocity.y, rig.velocity.x) * Mathf.Rad2Deg;
    transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    Vector3 targetVector = nextDestination - transform.position;
    rig.velocity = new Vector2(Mathf.Lerp(0, targetVector.normalized.x * speed, 0.8f), Mathf.Lerp(0, targetVector.normalized.y * speed, 0.8f));

  }

  void GenerateNextDestination()
  {
    nextDestination = center + new Vector3((Random.value - .5f) * size.x, (Random.value - .5f) * size.y, 0);
  }
}
