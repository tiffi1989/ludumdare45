﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RoundCounter : MonoBehaviour {

  private PlayerState ps;
  public EnemyController[] ec;
  public Text laptext;
  public Text positionText;
  private int counter;
  public GameObject[] wayPoints;

	// Use this for initialization
	void Start () {
    ps = GameObject.Find("Player").GetComponent<PlayerState>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
    counter++;
    if(counter > 20)
    {
      counter = 0;
      checkWinCondition();
      laptext.text = "Lap " + ps.lap + "/3";
      positionText.text = getPlayerPosition() + "/9";
    }
	}

  private void checkWinCondition()
  {
    if(ps.lap > 3 && getPlayerPosition() == 1)
    {
      SceneManager.LoadScene("Win");
    }
    if (ps.lap > 3 && getPlayerPosition() != 1)
    {
      SceneManager.LoadScene("Loose");
    }
  }

  public int getPlayerLap()
  {
    return ps.lap;
  }

  public int getPlayerPosition()
  {
    int position = 1;
    List<EnemyController> enemiesEqualToPlayer = new List<EnemyController>();
    int playerScore = ps.lap * 10 + ps.amountOfWaypointsThisLap;
    foreach(EnemyController enemy in ec)
    {
      if (enemy.lap * 10 + enemy.amountOfWaypointsThisLap > playerScore)
        position++;
      else if (enemy.lap * 10 + enemy.amountOfWaypointsThisLap == playerScore)
        enemiesEqualToPlayer.Add(enemy);
    }
    if(enemiesEqualToPlayer.Count > 0)
    {
      GameObject nextWayPoint = wayPoints.Length < ps.amountOfWaypointsThisLap ? wayPoints[ps.amountOfWaypointsThisLap] : wayPoints[0];
      float playerDistance = Vector3.Distance(ps.transform.position, nextWayPoint.transform.position);
      foreach(EnemyController enemy in enemiesEqualToPlayer)
      {
        if (Vector3.Distance(nextWayPoint.transform.position, enemy.transform.position) < playerDistance)
          position++;
      }
    }
    return position;
  }
}
